package ictgradschool.industry.lab15.ex01;

import java.util.List;
import java.util.ArrayList;

public class NestingShape extends Shape {
    private List<Shape> children = new ArrayList<>();
    public NestingShape(){
        super();
    }


    public NestingShape(int x, int y){
        super(x,y);
    }



    public NestingShape(int x, int y, int deltaX, int deltaY){
        super(x,y, deltaX, deltaY);
    }


    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height){
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height){
        super.move(width, height);
        for(Shape s : children) {
            s.move(fWidth, fHeight);
        }
    }

    public void paint(Painter painter){
        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.translate(fX, fY);
        for(Shape s : children) {
            s.paint(painter);
        }
        painter.translate(-fX, -fY);
    }

    public void add(Shape child) throws IllegalArgumentException{
        if(child.parent != null){
            throw new IllegalArgumentException("There is already a shape called" + child);
        }else if(child.fX + child.fWidth >= this.fWidth || child.fY + child.fHeight >= this.fHeight || child.fX < 0 || child.fY < 0){
            throw new IllegalArgumentException("This shape is too big");
        }else{
            child.setParent(this);
            children.add(child);
        }
    }


    public void remove(Shape child){
        if(children.contains(child)) {
            children.remove(child);
            child.setParent(null);
        }
    }


    public Shape shapeAt(int index) throws IndexOutOfBoundsException{
        return children.get(index);
    }


    public int shapeCount(){
        return (children.size());
    }

    public int indexOf(Shape child){
        return children.indexOf(child);

    }


    public boolean contains(Shape child) {
        return children.contains(child);

    }

}
