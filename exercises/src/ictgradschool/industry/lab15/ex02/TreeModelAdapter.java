package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class that implements the TreeModel interface by delegating to Directory
 * and File objects that form a hierarchical representation of a filestore.
 * This class is an object adapter.
 *
 */
public class TreeModelAdapter implements TreeModel {

	private NestingShape adaptee;

	private List<TreeModelListener> treeModelListeners;

	/**
	 * Creates a TreeModelAdapter object with the adaptee parameter, root.
	 */
	public TreeModelAdapter(NestingShape root) {
		adaptee = root;
		treeModelListeners = new ArrayList<TreeModelListener>();
	}

	/**
	 * Returns a reference to the root of the filestore.
	 */
	public Object getRoot() {
		return adaptee;
	}

	/**
	 * Returns the number of children (File/Directory objects) parameter parent
	 * has. parent is expected to point to a Directory object contained within
	 * the adaptee (root) Directory.
	 */
	public int getChildCount(Object parent) {
		int result = 0;
		Shape file = (Shape) parent;

		if (file instanceof NestingShape) {
			NestingShape shapes= (NestingShape) file;
			result = shapes.shapeCount();
		}
		return result;
	}

	/**
	 * Returns true if node refers to a Directory object, false otherwise.
	 */
	public boolean isLeaf(Object node) {
		return !(node instanceof NestingShape);
	}

	/**
	 * Adds the TreeModelListener parameter to this TreeModel's list of
	 * listeners.
	 */
	public void addTreeModelListener(TreeModelListener l) {
		treeModelListeners.add(l);
	}

	/**
	 * Removes the TreeModelListener parameter from this TreeModel's list of
	 * listeners.
	 */
	public void removeTreeModelListener(TreeModelListener l) {
		treeModelListeners.remove(l);
	}

	/**
	 * Returns child File/Directory object that is stored within the parent
	 * parameter at the position specified by the index parameter.
	 */
	public Object getChild(Object parent, int index) {
		Object result = null;

		if (parent instanceof NestingShape) {
			NestingShape s = (NestingShape) parent;
			result = s.shapeAt(index);
		}
		return result;
	}

	/**
	 * Returns the index position at which the child File/Directory parameter
	 * is stored within the Directory object that parameter parent is expected
	 * to refer. This method returns -1 if the object pointed to by the child
	 * parameter is not actually a child of parent.
	 */
	public int getIndexOfChild(Object parent, Object child) {

		return ((NestingShape) parent).indexOf((Shape)child);

	}

	public void valueForPathChanged(TreePath path, Object newValue) {
		/* No implementation required. */
	}
}